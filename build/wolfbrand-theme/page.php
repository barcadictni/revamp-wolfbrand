<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WolfBrand_Dashboard
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="main__dashboard">
		    <div class="main__dashboard--sidebar">

					<?php if(is_user_logged_in()): ?>

					<div class="main__dashboard--user">
						<div class="icon">
							<?php echo substr($current_user->user_login, 0,1); ?>
						</div>
						<div class="main__dashboard--userDetails">
							<h4>Welcome</h4>
							<h2>
								<?php echo $current_user->user_login; ?>
							</h2>
						</div>

						<a href="#dashboard" id="hamburguer-btn"><i class="fa fa-bars" aria-hidden="true"></i></a>
					</div>

					<div class="main__dashboard--menu" id="dashboard">

						<?php
							wp_nav_menu(array('menu' => 'dashboard'));
						?>

					</div><!-- END MENU DASHBOARD -->

				<?php endif; ?>

		    </div><!-- END SIDEBAR -->

		    <div class="main__dashboard--content">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', 'page' );


		endwhile; // End of the loop.
		?>
				</div>

			</div><!-- END MAIN CONTAINER DASHBOARD -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
