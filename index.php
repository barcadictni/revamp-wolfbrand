<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WolfBrand_Dashboard
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


			<div class="main__dashboard">
		    <div class="main__dashboard--sidebar">

					<?php if(is_user_logged_in()): ?>

					<div class="main__dashboard--user">
						<div class="icon">
							<?php echo substr($current_user->user_login, 0,1); ?>
						</div>
						<div class="main__dashboard--userDetails">
							<h4>Welcome</h4>
							<h2>
								<?php echo $current_user->user_login; ?>
							</h2>
						</div>

						<a href="#dashboard" id="hamburguer-btn"><i class="fa fa-bars" aria-hidden="true"></i></a>
					</div>

					<div class="main__dashboard--menu" id="dashboard">

						<?php
							wp_nav_menu(array('menu' => 'dashboard'));
						?>

					</div><!-- END MENU DASHBOARD -->

				<?php endif; ?>

		    </div><!-- END SIDEBAR -->

		    <div class="main__dashboard--content">





		<?php
		if ( have_posts() ) :



			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
				</div>

			</div><!-- END MAIN CONTAINER DASHBOARD -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
