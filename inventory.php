<?php
/*
* Template Name: Scooters Inventory
*/

get_header();
?>

<script type="text/javascript">
  /* Getting Scooters using ajax from the main site */

  jQuery(document).ready(function($){



  $.ajax({
   url: 'https://www.wolfbrandscooters.com/wp-json/wp/v2/scooters/',
   data: {
      format: 'json',
      per_page: 20
   },
   error: function() {
      $('#info').html('<p>An error has occurred</p>');
   },

   success: function(data) {

     var tableStart = "<table class='custom-table'>",
         tableHead = "<thead>" + "<tr><th>SKU</th><th>Color</th><th>Stock</th></tr>" + "</thead>",
         tableBodyStart = "<tbody>",
         tableRowStart = "<tr>",
         tableRowEnd = "</tr>",
         tableBodyEnd = "</tbody>",
         tableEnd = "</table>";

     data.forEach(function(el){
       //console.log(el);

       if(el.scooter_category[0] == 19){

         var rows = [];


      var title = "<h3>" + el.title.rendered + "</h3>";

      el.acf.galleries.forEach(function(currentScooter){
          rows.push("<tr><td>" + currentScooter.sku + "</td>" + "<td><div class='scooterColor' style='background-color:" + currentScooter.color + ";'>" + "</td>" + "<td>" + currentScooter.stock + "</td></tr>");
      });


      var content = "<div>" + tableStart + tableHead + tableBodyStart + rows + tableBodyEnd + tableEnd + "</div>";



      $("#info .50cc #accordion").append(title + content);

    }else{

      var rowsTwo = [];


   var title = "<h3>" + el.title.rendered + "</h3>";

   el.acf.galleries.forEach(function(currentScooter){
       rowsTwo.push("<tr><td>" + currentScooter.sku + "</td>" + "<td><div class='scooterColor' style='background-color:" + currentScooter.color + ";'>" + "</td>" + "<td>" + currentScooter.stock + "</td></tr>");
   });


   var content = "<div>" + tableStart + tableHead + tableBodyStart + rowsTwo + tableBodyEnd + tableEnd + "</div>";



   $("#info .150cc #accordion2").append(title + content);

    }

     });

    /*$("#info").append(data);
    console.log(data);*/

    $( "#accordion, #accordion2" ).accordion();
   },
   type: 'GET'
});




  });
</script>

<div id="primary" class="content-area">
  <main id="main" class="site-main">


    <div class="main__dashboard">
      <div class="main__dashboard--sidebar">

        <?php if(is_user_logged_in()): ?>

        <div class="main__dashboard--user">
          <div class="icon">
            <?php echo substr($current_user->user_login, 0,1); ?>
          </div>
          <div class="main__dashboard--userDetails">
            <h4>Welcome</h4>
            <h2>
              <?php echo $current_user->user_login; ?>
            </h2>
          </div>

          <a href="#dashboard" id="hamburguer-btn"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div><!-- MAIN DASHBOARD USER FINISH -->

        <div class="main__dashboard--menu" id="dashboard">

          <?php
            wp_nav_menu(array('menu' => 'dashboard'));
          ?>

        </div><!-- END MENU DASHBOARD -->

      <?php endif; ?>
      </div><!-- END SIDEBAR -->

      <div class="main__dashboard--content">

        <?php
    		while ( have_posts() ) :
    			the_post();

    			get_template_part( 'template-parts/content', 'page' );


    		endwhile; // End of the loop.
    		?>

      <div class="info" id="info">

        <h2>50cc Scooters</h2>
        <div class="50cc">


          <!-- TESTING ACCORDION -->

          <div id="accordion"></div>


        </div>

        <h2>150cc Scooters</h2>
        <div class="150cc">
          <div id="accordion2"></div>

        </div>

      </div>
</div>

</div><!-- END MAIN CONTAINER DASHBOARD -->
</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();

?>
