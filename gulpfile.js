var gulp = require('gulp'),
	postcss = require('gulp-postcss'),
  rucksack = require('rucksack-css'),
  cssnext = require('postcss-cssnext'),
	precss = require('precss'),
	fontmagician = require('postcss-font-magician'),
  cssnested = require('postcss-nested'),
  mixins = require('postcss-mixins'),
  atImport = require('postcss-import'),
  lost = require('lost'),
  mqpacker = require('css-mqpacker'),
  csswring = require('csswring'),
	zip = require('gulp-zip'),
	del = require('del'),
	browserSync = require('browser-sync').create();
	//Files to src and ingnore for gulp build
	var build_files = [
	  '**',
		'!.DS_Store',
	  '!node_modules',
	  '!node_modules/**',
	  '!bower_components',
	  '!bower_components/**',
		'!build',
	  '!build/**',
	  '!dist',
	  '!dist/**',
	  '!sass',
	  '!sass/**',
	  '!.git',
	  '!.git/**',
	  '!package.json',
	  '!.gitignore',
	  '!gulpfile.js',
	  '!.editorconfig',
	  '!.jshintrc'
	];

// Development Server
gulp.task('server', function(){

  var files = [
    './style.css',
    './*.php'
    ];

	browserSync.init(files, {
			proxy: 'localhost:8888/revampWolfbrand',
      notify: true
		});
	});

	gulp.task('build-copy', function() {
	  return gulp.src(build_files)
	    .pipe(gulp.dest('build/wolfbrand-theme'));
	});

	gulp.task('build-clean', function() {
  	del(['build/**/*']);
	});

	gulp.task('build-zip', function() {
  return gulp.src('build/**/*')
    .pipe(zip('wolfbrand-theme.zip'))
    .pipe(gulp.dest('dist'));
	});

// Task for css
gulp.task('css', function() {
    // content

    var processor = [
			atImport(),
		  precss,
		  lost(),
		  rucksack(),
		  cssnext({ browsers: ['> 5%','ie > 8'] }),
		  mqpacker({ sort: true}),
		  fontmagician/*,
		  csswring()*/

    ];

    return gulp.src('css/style.css')
      .pipe(postcss(processor))
      .pipe(gulp.dest('./'))
      .pipe(browserSync.stream())
});

// Task for watch changes
gulp.task('watch', function() {
    // content
    gulp.watch('css/*.css', ['css']);
    gulp.watch('*.php').on('change', browserSync.reload);
});

gulp.task('default',['watch', 'server']);
